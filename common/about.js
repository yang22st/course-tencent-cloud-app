/**
 * NOTE:尾部不要加"/"
 */
//export const apiBaseUrl = 'https://ctc.koogua.com/api'
export const apiBaseUrl = 'http://10.86.41.156/api'

export const appInfo = {
	name: '酷瓜云课堂',
	alias: 'ctc-app',
	link: 'https://gitee.com/koogua/course-tencent-cloud-app',
	version: '1.0.0'
}
